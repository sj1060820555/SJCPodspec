
Pod::Spec.new do |s|
s.name             = 'SJUIKit'
s.version          = '0.0.2'
s.summary          = 'SJUIKit for UI'

s.description      = "最基础的UI工具最基础的UI工具最基础的UI工具具最基础的UI工具"
s.homepage         = 'https://gitlab.com/sj1060820555/'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
#工程的证书
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'sj1060820555@163.com' => 'sj1060820555@163.com' }
#工程的git地址
s.source           = { :git => 'https://gitlab.com/sj1060820555/sjuikit.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '8.0'

#工程需要引入的文件
s.source_files = 'SJUIKit/Classes/SJUIKitHeader.h'
s.public_header_files = "SJUIKit/Classes/SJUIKitHeader.h"
# s.resource_bundles = {
#   'SJUIKit' => ['SJUIKit/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
s.dependency 'Masonry','~> 1.1.0'

#---- Subspec ----------

#s.subspec 'Tool' do |ss|
#ss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end

#s.subspec 'View' do |ss|
#ss.source_files  = 'SJUIKit/Classes/View.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/View/SJUIViewHeader.h';
#ss.subspec 'Tool' do |sss|
#sss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#sss.public_header_files = 'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end
#end

#s.subspec 'Label' do |ss|
#ss.source_files = 'SJUIKit/Classes/Label.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/Label/SJUILabelHeader.h';
#ss.subspec 'Tool' do |sss|
#sss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#sss.public_header_files =  'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end
#end

#s.subspec 'Button' do |ss|
#ss.source_files = 'SJUIKit/Classes/Button.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/Button/SJUIButtonHeader.h';
#ss.subspec 'Tool' do |sss|
#sss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#sss.public_header_files =  'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end
#end

#s.subspec 'TextField' do |ss|
#ss.source_files = 'SJUIKit/Classes/TextField.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/TextField/SJUITextFieldHeader.h';
#ss.subspec 'Tool' do |sss|
#sss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#sss.public_header_files =  'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end
#end

#s.subspec 'ViewControl' do |ss|
#ss.source_files = 'SJUIKit/Classes/ViewControl.{h,m}';
#ss.public_header_files =  'SJUIKit/Classes/ViewControl/SJUIViewHeader.h';
#ss.subspec 'Tool' do |sss|
#sss.source_files = 'SJUIKit/Classes/Tool.{h,m}';
#sss.public_header_files =  'SJUIKit/Classes/Tool/SJUIToolHeader.h';
#end
#end

end

