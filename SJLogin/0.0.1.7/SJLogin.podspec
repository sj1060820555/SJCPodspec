#
# Be sure to run `pod lib lint SJLogin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SJLogin'
  s.version          = '0.0.1.7'
  s.summary          = 'A short description of SJLogin.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/sj106082055'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'sj1060820555@163.com' => 'sj106080555@163.com' }
  s.source           = { :git => 'https://gitlab.com/sj1060820555/SJLogin.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'SJLogin/Classes/*.h'
  
  s.subspec 'VC' do |ss|
   
      ss.source_files = 'SJLogin/Classes/VC/**/*'
      ss.dependency 'SJUIKit'
      
       ss.resource_bundles = {
           'Login_login_Image_Bundle' => ['SJLogin/Assets/*.{xcassets,bundle}']
       }
  end
  
  s.subspec 'viewModel' do |ss|
      ss.source_files = 'SJLogin/Classes/viewModel/*'
  end
  
  s.subspec 'model' do |ss|
      ss.source_files = 'SJLogin/Classes/model/*'
  end
  
  #s.subspec 'view' do |ss|
  #ss.source_files = 'SJLogin/Classes/view/*'
  #end
      
      
  
  # s.resource_bundles = {
  #   'SJLogin' => ['SJLogin/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
