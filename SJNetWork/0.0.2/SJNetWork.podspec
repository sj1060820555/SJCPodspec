#
# Be sure to run `pod lib lint SJNetWork.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SJNetWork'
  s.version          = '0.0.2'
  s.summary          = 'SJNetWork.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/sj1060820555'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'sj1060820555@163.com' => 'sj1060820555@163.com' }
  s.source           = { :git => 'https://gitlab.com/sj1060820555/SJNetWork.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  
  # s.resource_bundles = {
  #   'SJNetWork' => ['SJNetWork/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UIKit','Foundation'
  # s.dependency 'AFNetworking', '~> 2.3'

    s.source_files = 'SJNetWork/Classes/network/*h'

    s.subspec 'network' do |ns|
         ns.source_files = 'SJNetWork/Classes/network/net/**/*'
         ns.dependency 'AFNetworking', '~>3.2.1'
    end
    
    #s.subspec 'networkUseHandler' do |ns|
    #ns.dependency    'SJNetWork/network'
    # ns.source_files = 'SJNetWork/Classes/network/networkUseHandler/**/*'
    #end
    

    s.subspec 'tool' do |ns|
        ns.source_files = 'SJNetWork/Classes/network/tool/**/*'
    end
    
    

end
